# Group Project in TDT4240 - Software Architecture
<div align="center">

![GitHub Workflow Status (with event)](https://img.shields.io/github/actions/workflow/status/SverreNystad/progark/ci.yml)
![GitHub top language](https://img.shields.io/github/languages/top/SverreNystad/progark)
![GitHub language count](https://img.shields.io/github/languages/count/SverreNystad/progark)
[![Project Version](https://img.shields.io/badge/version-0.0.1-blue)](https://img.shields.io/badge/version-0.0.1-blue)


</div>

<details>
    <summary><strong> Table of Contents </strong></summary>

- [Group Project in TDT4240 - Software Architecture](#group-project-in-tdt4240---software-architecture)
  - [Introduction](#introduction)
  - [Installation and Setup](#installation-and-setup)
    - [Prerequisites](#prerequisites)
    - [Installing](#installing)
  - [How to Play](#how-to-play)
  - [Running the tests](#running-the-tests)
  - [Contributors](#contributors)

</details>

## Introduction


## Installation and Setup


### Prerequisites
<ul>
<details> <summary><b> Git </b> </summary>
  Git is a distributed version control system that is used to manage the source code of this project. It is essential for cloning the project from GitHub and collaborating with other developers.

  * Git - Version Control System
    * Download and install Git from the official [Git website](https://git-scm.com/downloads).
    * After installation, verify the installation by running ```git --version``` in your command line or terminal.
</details>
</ul>

<ul>
  <details> <summary><b> Java JDK 17 (Download from Oracle's website) </b></summary>
  This project requires Java JDK to be installed. The project is tested with JDK 17.

  * Java JDK 17 - Java Development Kit is essential for compiling and running Java applications.
    * Download and install it from [Oracle's Java JDK Download Page](https://www.oracle.com/java/technologies/downloads/#java17) or adopt an open-source JDK like AdoptOpenJDK.
    * After installation, verify the installation by running ```java -version``` and ```javac -version``` in your command line or terminal.
  </details>
</ul>
<ul>
  <details> 
  <summary><b> Gradle 6.7 </b></summary>
  Gradle is used as the build tool for this project. It automates the process of building, testing, and deploying the application.

  * Gradle 6.7 - Gradle brings advanced build toolkit to manage dependencies and other aspects of the build process.
    * You can download Gradle from the [Gradle Download Page](https://gradle.org/install/).
    * Alternatively, if you are using a Gradle Wrapper script (gradlew or gradlew.bat), you do not need to manually install Gradle, as the wrapper script will handle the installation for you.
    * To confirm that Gradle is properly installed, run ```gradlew -v``` in your command line or terminal which will display the installed Gradle version.
  </details>
</ul>

<ul>
  <details> 
    <summary><b>Android SDK (if you want to run the game on Android)</b></summary>
    When testing the Android app one can run it either by connecting your Android phone via USB to your computer, or you could use an Android emulator (virtual device). To do this, you need to have the Android SDK installed.
    details> 
    <summary><b> Android SDK (if you want to run the game on Android) </b></summary>
    To set up the Android SDK for running the game on an emulator, you need to create a file called `local.properties` in the root of the project and add the path to your SDK with the following line:
    
    echo sdk.dir=YOUR/ANDROID/SDK/PATH > local.properties

  </details> 
</ul>
<ul>
  <details>
    <summary><b>Firebase Secret Key (for multiplayer features)</b></summary>
    To enable multiplayer features in the game, you need to obtain a Firebase secret key. This key is necessary to authenticate and manage the game's online interactions securely.
    <ul> 
     Obtain your Firebase secret key from the Firebase console.
    </ul>
    <ul>
      Put the key into the asset` folder with the name 
      ``FirebaseSecretKey.json``
    </ul>
  </details>
</ul>

Ensure that both Java and Gradle are properly installed and configured in your system's PATH environment variable to be able to run the game. 

### Installing
To install the project, you can use the following commands:
```cmd
git clone https://gitlab.stud.idi.ntnu.no/sverrnys/besieged.git
```


## How to Play
The game can be played on both desktop and Android.

To start the game, you can start it on desktop using the following command:
```cmd
gradlew desktop:run
```


### Running in Android Studio
Open the root folder as a project in Android Studio and let it configure itself the first time you open it. It should then automatically make an Android run configuration, so all you have to do is press the green run button on the top bar. Please take a look at figure


![Alt text](image.png)

## Running the tests
To run the tests, you can use the following command:
```cmd
./gradlew test
```

## Contributors
<table>
  <tr>
    <td align="center">
        <a href="https://github.com/Jensern1">
            <img src="https://github.com/Jensern1.png?size=100" width="100px;" alt="Jens Martin Norheim Berget"/><br />
            <sub><b>Jens Martin Norheim Berget</b></sub>
        </a>
    </td>
    <td align="center">
      <a href="https://github.com/mvbryne">
            <img src="https://github.com/mvbryne.png?size=100" width="100px;" alt="Magnus Vesterøy Bryne"/><br />
            <sub><b>Magnus Vesterøy Bryne</b></sub>
        </a>
    </td>
    <td align="center">
        <a href="https://github.com/mattiastofte">
            <img src="https://github.com/mattiastofte.png?size=100" width="100px;" alt="Mattias Tofte"/><br />
            <sub><b>Mattias Tofte</b></sub>
        </a>
    </td>
    <td align="center">
        <a href="https://github.com/SverreNystad">
            <img src="https://github.com/SverreNystad.png?size=100" width="100px;"
            alt="Sverre Nystad"/><br />
            <sub><b>Sverre Nystad</b></sub>
        </a>
    </td>
    <td align="center">
        <a href="https://github.com/Artewald">
            <img src="https://github.com/Artewald.png?size=100" width="100px;" alt="Tim Matras"/><br />
            <sub><b>Tim Matras</b></sub>
        </a>
    </td>
    <td align="center">
        <a href="https://github.com/tobiasfremming">
            <img src="https://github.com/tobiasfremming.png?size=100" width="100px;"/><br />
            <sub><b>Tobias Fremming</b></sub>
        </a>
    </td>
  </tr>
</table>
